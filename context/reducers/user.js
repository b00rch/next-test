/* eslint-disable no-case-declarations */
import { SET_USER } from '../actions/user';

let user = {};
if (process.browser) {
  user = JSON.parse(localStorage.getItem('user'));
}

const initialState = user;

const reducer = (state, action) => {
  switch (action.type) {
    case SET_USER:
      return { ...action.user };
    default:
      return state;
  }
};

const userReducer = [reducer, initialState];

export default userReducer;
