import combineReducers from 'react-combine-reducers';
import user from './user';

const [reducer, initialState] = combineReducers({
  user,
});

export { reducer, initialState };
