import React, { createContext, useReducer } from 'react';
import { reducer, initialState } from './reducers';

const GlobalContext = createContext();

const ContextState = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GlobalContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export { GlobalContext, ContextState };
