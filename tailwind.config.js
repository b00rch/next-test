module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  theme: {
    extend: {
      backgroundImage: {
        'id-card-cover': "url('/images/id-card-cover-other.png')",
        'id-card-back': "url('/images/id-card-back-other.png')",
        'id-card-inhand': "url('/images/id-card-inhand-other.png')",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
