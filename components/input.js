export default function InputComponent(props) {
  return (
    <>
      <label className="mt-4">{props.label}</label>
      <input className="rounded border-2 border-gray-200" {...props} />
    </>
  );
}
