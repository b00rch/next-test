export default function ButtonComponent(props) {
  const classes = `${props.disabled ? 'bg-gray-300' : 'bg-green-300'} rounded mt-4 p-2`;

  return <button className={classes} {...props} />;
}
