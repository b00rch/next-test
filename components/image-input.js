export default function ButtonComponent({ label, image, size, onChange }) {
  let classes = 'rounded overflow-hidden bg-no-repeat bg-contain bg-center';
  const style = { ...size };

  if (typeof image === 'string') {
    classes += ` ${image}`;
  } else if (typeof image === 'object') {
    style.backgroundImage = 'url(' + URL.createObjectURL(image) + ')';
  }

  return (
    <>
      <label className="mt-4">{label}</label>
      <div className={classes} style={style}>
        <input type="file" className="opacity-0 cursor-pointer" style={size} onChange={onChange} />
      </div>
    </>
  );
}
