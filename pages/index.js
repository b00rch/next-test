// import Image from 'next/image';
import { useState, useContext } from 'react';
import Router from 'next/router';

import Layout from '../layouts/layout';
import Input from '../components/input';
import Button from '../components/button';
import { SET_USER } from '../context/actions/user';
import { GlobalContext } from '../context';

export default function Home() {
  const { dispatch } = useContext(GlobalContext);

  const [registerNo, setRegister] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const checkRegisterNo = async (event) => {
    event.preventDefault();
    if (!registerNo) {
      return;
    }

    setIsLoading(true);

    try {
      const res = await fetch(`/api/check?registerNo=${registerNo}`);
      const data = await res.json();
      const { user } = data;

      if (user && user.frontURL && user.selfieURL) {
        dispatch({
          type: SET_USER,
          user,
        });
        Router.push('/verification');
        return;
      }
    } catch (err) {
      console.error(err);
    }
    Router.push('/register?registerNo=' + registerNo);
  };

  return (
    <Layout>
      <div className="bg-white p-4 m-12 rounded">
        <div className="text-3xl">Check register No</div>
        <form onSubmit={checkRegisterNo}>
          <div className="flex flex-col">
            <Input
              label="Register No"
              id="name"
              name="name"
              type="text"
              value={registerNo}
              autoComplete="name"
              required
              onChange={(e) => setRegister(e.target.value)}
            />

            <Button type="submit" disabled={isLoading}>
              {isLoading ? 'Checking' : 'Check'}
            </Button>
          </div>
        </form>
      </div>
    </Layout>
  );
}
