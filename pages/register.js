import { useState, useContext } from 'react';
import Router, { useRouter } from 'next/router';

import Layout from '../layouts/layout';
import Button from '../components/button';
import ImageInput from '../components/image-input';
import { SET_USER } from '../context/actions/user';
import { GlobalContext } from '../context';

export default function RegisterForm() {
  const { dispatch } = useContext(GlobalContext);

  const router = useRouter();
  const { registerNo } = router.query;

  const [frontImage, setFrontImage] = useState(null);
  const [selfieImage, setSelfieImage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const onChangeFront = async (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setFrontImage(null);
      return;
    }
    setFrontImage(e.target.files[0]);
  };

  const onChangeSelfie = async (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelfieImage(null);
      return;
    }
    setSelfieImage(e.target.files[0]);
  };

  const saveUser = async (event) => {
    event.preventDefault();

    setIsLoading(true);

    var data = new FormData();
    data.append('registerNo', registerNo);
    data.append('frontFile', frontImage);
    data.append('selfieFile', selfieImage);

    const res = await fetch('/api/save', {
      method: 'POST',
      body: data,
    });
    const user = await res.json();
    console.log(user);

    dispatch({
      type: SET_USER,
      user,
    });
    Router.push('/verification');
    // setIsLoading(false);
  };

  return (
    <Layout>
      <div className="bg-white p-4 m-12 rounded">
        <div className="text-3xl">Register form</div>
        <form onSubmit={saveUser}>
          <div className="flex flex-col">
            <div className="mt-4">Your register no: {registerNo}</div>
            <div>please upload ID card images</div>
            <ImageInput
              label="ID card front"
              image={frontImage || 'bg-id-card-cover'}
              size={{ width: 370, height: 208 }}
              onChange={onChangeFront}
            />

            <ImageInput
              label="Selfie with ID card"
              image={selfieImage || 'bg-id-card-inhand'}
              size={{ width: 370, height: 208 }}
              onChange={onChangeSelfie}
            />

            <Button type="submit" disabled={isLoading}>
              {isLoading ? 'Saving' : 'Save'}
            </Button>
          </div>
        </form>
      </div>
    </Layout>
  );
}
