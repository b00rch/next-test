import '../styles/global.css';
import { ContextState } from '/context';

export default function App({ Component, pageProps }) {
  return (
    <ContextState>
      <Component {...pageProps} />
    </ContextState>
  );
}
