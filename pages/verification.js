import { useContext } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import Layout from '../layouts/layout';
import Button from '../components/button';
import { GlobalContext } from '../context';
import { SET_USER } from '../context/actions/user';

export default function RegisterForm() {
  const { state, dispatch } = useContext(GlobalContext);

  console.log(state);
  const { user } = state;
  console.log(user);

  const verifyUser = async (event) => {
    event.preventDefault();
    console.log(user);

    const res = await fetch('/api/verify', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const newUser = await res.json();
    console.log(newUser);

    dispatch({
      type: SET_USER,
      user: newUser,
    });
  };

  return (
    <Layout>
      <div className="bg-white p-4 m-12 rounded">
        <div className="flex justify-between items-center">
          <div className="text-3xl">ID Card verification</div>
          <Link href="/">
            <a className="text-sm">Back</a>
          </Link>
        </div>
        {user ? (
          <form onSubmit={verifyUser}>
            <div className="flex flex-col">
              <div>{user.registerNo}</div>
              <div>{user.lastName}</div>
              <div>{user.firstName}</div>
              {user.isVerified ? (
                <div className="text-green-600">Verified</div>
              ) : (
                <div className="text-red-600">Not verified</div>
              )}
              <div className="w-full mt-2">
                <Image
                  alt="ID card front"
                  src={user.frontURL}
                  width={370}
                  height={208}
                  layout="responsive"
                  quality={100}
                />
              </div>
              <div className="w-full mt-2">
                <Image
                  alt="Selfie with ID card"
                  src={user.selfieURL}
                  width={370}
                  height={208}
                  layout="responsive"
                  quality={100}
                />
              </div>

              <Button type="submit">Verify</Button>
            </div>
          </form>
        ) : (
          <div>No user info</div>
        )}
      </div>
    </Layout>
  );
}
