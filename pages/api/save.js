import fs from 'fs';
import nextConnect from 'next-connect';
import database from '../../middleware/database';
import { analyzeImage, saveFileToS3 } from '../../utils/aws';
import { formDataParser } from '../../utils/formDataParser';

const handler = nextConnect();
handler.use(database);

handler.post(async (req, res) => {
  try {
    const { fields, files } = await formDataParser(req);
    const { frontFile, selfieFile } = files;
    const { registerNo } = fields;
    console.log(registerNo);

    if (!fs.existsSync(frontFile.filepath)) {
      new Error('ID Card front image is not uploaded');
    }
    if (!fs.existsSync(selfieFile.filepath)) {
      new Error('Selfie image is not uploaded');
    }
    // console.log(Object.keys(frontFile));

    const frontURL = await saveFileToS3(frontFile);
    const selfieURL = await saveFileToS3(selfieFile);

    let user = {
      registerNo,
      frontURL,
      selfieURL,
      frontFileName: frontFile.originalFilename,
      selfieFileName: selfieFile.originalFilename,
    };
    // return res.status(200).json({ frontURL, selfieURL });

    const nameData = await analyzeImage(frontFile, registerNo);
    console.log(nameData);

    if (nameData.isMatchRegister) {
      user = { ...user, ...nameData };
    }

    const users = req.db.collection('users');

    const query = { registerNo };
    const update = { $set: user };
    const options = { upsert: true };
    users.updateOne(query, update, options);

    res.json(user);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: 'aldaa' });
  }
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
