// import fs from 'fs';
// import formidable from 'formidable';
import nextConnect from 'next-connect';
import database from '../../middleware/database';
import { compareImage } from '../../utils/aws';

const handler = nextConnect();
handler.use(database);

// const post = async (req, res) => {
//   const form = new formidable.IncomingForm();

//   return new Promise((resolve, reject) => {
//     form.parse(req, async (err, fields, files) => {
//       if (err) {
//         reject(err);
//       }
//       // console.log(files.frontFile);
//       const { frontFile, selfieFile } = files;
//       if (!fs.existsSync(frontFile.filepath)) {
//         reject(new Error('ID Card front image is not uploaded'));
//       }
//       if (!fs.existsSync(selfieFile.filepath)) {
//         reject(new Error('Selfie image is not uploaded'));
//       }
//       // console.log(Object.keys(frontFile));
//       const frontURL = await saveFileToS3(frontFile);
//       const selfieURL = await saveFileToS3(selfieFile);
//       resolve({ frontURL, selfieURL });
//     });
//   });
// };

handler.post(async (req, res) => {
  try {
    console.log(req.body);
    const { frontFileName, selfieFileName, registerNo } = req.body;
    const result = await compareImage(
      {
        S3Object: {
          Bucket: process.env.AWS_BUCKET_NAME,
          Name: frontFileName,
        },
      },
      {
        S3Object: {
          Bucket: process.env.AWS_BUCKET_NAME,
          Name: selfieFileName,
        },
      }
    );
    console.log(result);
    const { FaceMatches, UnmatchedFaces } = result;

    const user = { ...req.body };
    console.log(user);

    if (
      Array.isArray(FaceMatches) &&
      FaceMatches.length > 0 &&
      Array.isArray(UnmatchedFaces) &&
      UnmatchedFaces.length === 0
    ) {
      user.isVerified = true;
    }

    const users = req.db.collection('users');

    const query = { registerNo };
    const update = { $set: user };
    const options = { upsert: true };
    users.updateOne(query, update, options);

    return res.json(user);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: 'aldaa' });
  }
});

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '2mb',
    },
  },
};

export default handler;
