import nextConnect from 'next-connect';
import database from '../../middleware/database';

const handler = nextConnect();
handler.use(database);

handler.get(async (req, res) => {
  const { registerNo } = req.query;
  console.log(registerNo);

  try {
    const users = req.db.collection('users');
    const user = await users.findOne({ registerNo });
    console.log(user);

    res.json({ user });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: 'aldaa' });
  }
  // res.status(200).json({ ok: 'ok' });
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
