This is a starter template for [Learn Next.js](https://nextjs.org/learn).

# ID Card verification test

## Requirement
1. node v14 or greater.

## Developing locally

1. Clone this repo and `cd` into it
1. Install all dependencies `npm install `
1. Copy `.env.local.example` to `.env.local`
1. Configure your AWS access and MongoDB connection string
1. Run development: `npm run dev`
