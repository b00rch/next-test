import { MongoClient } from 'mongodb';
import nextConnect from 'next-connect';

// console.log(process.env.MONGO_DB_URI);
export const client = new MongoClient(process.env.MONGO_DB_URI);

async function database(req, res, next) {
  try {
    await client.connect();
    req.dbClient = client;
    req.db = client.db(process.env.MONGO_DB_NAME);
  } catch (err) {
    return next(new Error('Can not connect DB'));
  }
  return next();
}

const middleware = nextConnect();

middleware.use(database);

export default middleware;
