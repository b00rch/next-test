var c_to_l = {
  'А':'A', 'а':'a',	'Б':'B', 'б':'b',	'В':'V', 'в':'v',	'Г':'G', 'г':'g',	'Д':'D', 'д':'d',
  'Е':'Ye', 'е':'ye', 'Ё':'Yo', 'ё':'yo', 'Ж':'J', 'ж':'j', //З з	И и
  // Й й	К к	Л л	М м	Н н
  // О о	Ө ө	П п	Р р	С с
  // Т т	У у	Ү ү	Ф ф	Х х
  // Ц ц	Ч ч	Ш ш	Щ щ	ъ
  // ы	ь	Э э	Ю ю	Я я
};

var l_to_c = {
  'А':'A', 'a':'а', 'B':'Б', 'b':'б', 'V':'В', 'v':'в',	'G':'Г', 'g':'г',	'D':'Д', 'd':'д',
  
}

export const transliterateToLatin = (word) =>
  word
    .split('')
    .map((char) => c_to_l[char] || char)
    .join('');

export const transliterateToCyrillic = (word) =>
  word
    .split('')
    .map((char) => l_to_c[char] || char)
    .join('');
