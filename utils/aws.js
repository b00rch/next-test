import AWS from 'aws-sdk';
import fs from 'fs';
import { transliterateToCyrillic, transliterateToLatin } from './transliterate';

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const s3 = new AWS.S3();
const textract = new AWS.Textract();
const rekognition = new AWS.Rekognition();

const getName = (blocks, block, index) => {
  console.log(`${index} Text: ${block.Text};`, block);

  const monNameBlock = blocks[index + 1];
  const engNameBlock = blocks[index + 2];

  console.log(`mon Text: ${monNameBlock.Text};`, monNameBlock);
  console.log(`eng Text: ${engNameBlock.Text};`, engNameBlock);
  console.log('\n\n');

  // if (monNameBlock.Confidence < 50 && engNameBlock.Confidence > 60) {
  return engNameBlock.Text;
  // }
  // return '';
};

export const saveFileToS3 = (file) => {
  const fileContent = fs.readFileSync(file.filepath);

  const params = {
    Bucket: process.env.AWS_BUCKET_NAME,
    Key: file.originalFilename,
    Body: fileContent,
  };

  return new Promise((resolve, reject) => {
    s3.upload(params, (error, data) => {
      if (error) {
        reject(error);
      }
      resolve(data.Location);
    });
    // resolve(params);
  });
};

export const analyzeImage = async (file, registerNo) => {
  const fileContent = fs.readFileSync(file.filepath);
  const params = {
    Document: {
      /* required */
      Bytes: fileContent,
    },
    FeatureTypes: ['FORMS'],
  };

  const request = textract.analyzeDocument(params);
  const data = await request.promise();
  const keyValues = {
    isMatchRegister: false,
  };

  if (data && data.Blocks) {
    data.Blocks.forEach((block, index) => {
      if (block.BlockType !== 'LINE') {
        return;
      }

      console.log(`${index} Text: ${block.Text};`);

      if (typeof block.Text === 'string' && block.Text.includes(transliterateToLatin(registerNo))) {
        keyValues.isMatchRegister = true;
      }

      if (
        typeof block.Text === 'string' &&
        block.Text.includes('Family name') &&
        block.Confidence > 80
      ) {
        keyValues.latinFamilyName = getName(data.Blocks, block, index);
        keyValues.cyrillicFamilyName = transliterateToCyrillic(keyValues.latinFamilyName);
      }

      if (
        typeof block.Text === 'string' &&
        block.Text.includes('Surname') &&
        block.Confidence > 80
      ) {
        keyValues.latinLastName = getName(data.Blocks, block, index);
        keyValues.cyrillicLastName = transliterateToCyrillic(keyValues.latinLastName);
      }

      if (
        typeof block.Text === 'string' &&
        block.Text.includes('Given name') &&
        block.Confidence > 80
      ) {
        keyValues.latinFirstName = getName(data.Blocks, block, index);
        keyValues.cyrillicFirstName = transliterateToCyrillic(keyValues.latinFirstName);
      }
    });
  }

  return keyValues;
};

export const compareImage = async (SourceImage, TargetImage) => {
  const params = {
    SimilarityThreshold: 90,
    SourceImage,
    TargetImage,
  };

  return new Promise((resolve, reject) => {
    rekognition.compareFaces(params, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      // an error occurred

      resolve(data); // successful response
    });
  });
};
